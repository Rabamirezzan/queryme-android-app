package com.queryme.androidapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		System.out.println("QueryMe Service Broadcast: " + intent.getAction());
//		context.registerReceiver(this,
//				new IntentFilter(Intent.ACTION_TIME_TICK));
		boolean boot = intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
				| intent.getAction().equals(Intent.ACTION_TIME_TICK)
				| intent.getAction().equals(
						"android.net.conn.CONNECTIVITY_CHANGE")
				| intent.getAction().equals(
						"android.net.wifi.WIFI_STATE_CHANGED")
				| intent.getAction().equals("android.net.wifi.STATE_CHANGE");

		if (boot) {
			QueryMeService.start(context);
		}

	}

}
