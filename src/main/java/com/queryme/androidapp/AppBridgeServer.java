package com.queryme.androidapp;


import com.queryme.androidlib.BridgeServerService;

public class AppBridgeServer extends BridgeServerService {

	public static final int PORT = 7777;
	public AppBridgeServer() {
		super("QueryMe");
	}

	@Override
	public int getPort() {

		return PORT;
	}

	@Override
	public String getDatabaseName() {

		return "";
	}

	@Override
	public int getDatabaseVersion() {

		return 0;
	}
	
	@Override
	public void onDestroy() {
		System.out.println("Destroy AppBridgeServer");
		super.onDestroy();
	}

}
