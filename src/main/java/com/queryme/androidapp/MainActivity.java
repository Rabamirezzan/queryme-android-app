package com.queryme.androidapp;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

//		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//		StrictMode.setThreadPolicy(policy);
		QueryMeService.start(this);
		System.out.println("Build.VERSION.SDK " + Build.VERSION.SDK_INT);
		System.out.println("Build.VERSION.RELEASE " + Build.VERSION.RELEASE);
		TextView ipView = (TextView) findViewById(R.id.ip);
		String ip = NetworkUtil.getIPAddress(true);
		String sIP = String.format("%s:%s", ip, AppBridgeServer.PORT);
		ipView.setText(sIP);
		
		

	}

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
                Toast.makeText(this, "Created by rramirezb", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onMenuItemSelected(featureId, item);

    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
