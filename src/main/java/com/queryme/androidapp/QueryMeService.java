package com.queryme.androidapp;

import android.content.Context;
import android.content.Intent;

public class QueryMeService {
	public static void start(Context context){
		Intent service = new Intent(context, AppBridgeServer.class);
		context.startService(service);
		System.out.println("QueryMe Service iniciado.");
	}
}
